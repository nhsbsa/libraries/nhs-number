package uk.nhs.nhsbsa.nhsnumber;

import java.beans.PropertyEditorSupport;

public class NhsNumberPropertyEditor extends PropertyEditorSupport {

    private static final String REGEX_ONLY_DIGIT_HYPHEN_SPACE = "^[0-9 -]+$";

    @Override
    public void setAsText(String text) {
        if(text != null && text.matches(REGEX_ONLY_DIGIT_HYPHEN_SPACE)) {
            text = text.replaceAll("[ -]", "");
        }

        setValue(text);
    }

    @Override
    public String getAsText() {
        String unformatted = (String) getValue();

        if(unformatted == null) return null;

        String formattedString = unformatted;

        if( !formattedString.matches(REGEX_ONLY_DIGIT_HYPHEN_SPACE) ) return formattedString;

        if(formattedString.length() > 3) {
            formattedString = addSpaceAtIndex(formattedString, 4);

            if(formattedString.length() > 7) {
                formattedString = addSpaceAtIndex(formattedString, 8);
            }
        }

        return formattedString;
    }

    private String addSpaceAtIndex(String string, int index) {
        StringBuilder builtStringBuilder = new StringBuilder();

        builtStringBuilder.append(string, 0, index-1);
        builtStringBuilder.append(" ");
        builtStringBuilder.append(string, index-1, string.length());

        return builtStringBuilder.toString();

    }
}
