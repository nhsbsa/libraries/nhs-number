package uk.nhs.nhsbsa.nhsnumber;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


class NhsNumberPropertyEditorTest {

    NhsNumberPropertyEditor editor;

    static final String DEFAULT_NHS_NUMBER = "250 570 1822";

    @Test
    void testNull() {
        check(null, null);
    }

    @Test
    void testOneSpaceCenter() {
        check("250 5701822", DEFAULT_NHS_NUMBER);
    }

    @Test
    void testSpaceAndOtherCharsCenter() {
        // contains non numbers so don't format
        check("25057 0#¢∞§", "25057 0#¢∞§");
    }

    @Test
    void testSpacesEverywhere() {
        check("             2 5 0 5 7 0 18 2 2   ", DEFAULT_NHS_NUMBER);
    }

    @Test
    void testHyphensEverywhere() {
        check("------2-5-0-5-7-0-18-2-2---", DEFAULT_NHS_NUMBER);
    }

    @Test
    void zeroDigits() {
        check("", "");
    }

    @Test
    void threeDigits() {
        check("123", "123");
    }

    @Test
    void fourDigits() {
        check("1234", "123 4");
    }


    @Test
    void sixDigits() {
        check("123456", "123 456");
    }

    @Test
    void sevenDigits() {
        check("1234567", "123 456 7");
    }

    @Test
    void nineDigits() {
        check("123456789", "123 456 789");
    }

    private void check(String input, String expectedFormatted) {
        editor = new NhsNumberPropertyEditor();
        editor.setAsText(input);
        String actualFormatted = editor.getAsText();
        assertEquals(expectedFormatted, actualFormatted);
    }
}
