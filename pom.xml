<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>3.2.4</version>
		<relativePath /> <!-- lookup parent from repository -->
	</parent>

	<groupId>uk.nhs.nhsbsa</groupId>
	<artifactId>nhs-number</artifactId>
	<version>0.0.0</version>
	<packaging>jar</packaging>

	<description>Support classes or NHS number</description>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<java.version>17</java.version>
		<maven.compiler.source>${java.version}</maven.compiler.source>
		<maven.compiler.target>${java.version}</maven.compiler.target>
		<sonar-maven-plugin.version>3.9.1.2184</sonar-maven-plugin.version>
		<sonar.projectKey>${project.groupId}:${project.artifactId}:${sonar.suffix}</sonar.projectKey>
		<sonar.projectName>${project.artifactId} ${sonar.suffix}</sonar.projectName>
		<sonar.projectVersion>${project.version}</sonar.projectVersion>
		<sonar.java.source>${java.version}</sonar.java.source>
		<joda.version>2.12.7</joda.version>
		<commons.collections.version>4.4</commons.collections.version>
	</properties>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-validation</artifactId>
		</dependency>

		<dependency>
			<groupId>joda-time</groupId>
			<artifactId>joda-time</artifactId>
			<version>${joda.version}</version>
		</dependency>

		<dependency>
			<groupId>com.fasterxml.jackson.datatype</groupId>
			<artifactId>jackson-datatype-joda</artifactId>
		</dependency>

		<!-- test -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-test</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-collections4</artifactId>
			<version>${commons.collections.version}</version>
		</dependency>
	</dependencies>

	<build>
		<pluginManagement>
			<plugins>

				<!-- distribute sources -->
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-source-plugin</artifactId>
					<version>3.3.0</version>
					<executions>
						<execution>
							<id>attach-sources</id>
							<phase>verify</phase>
							<goals>
								<goal>jar-no-fork</goal>
								<goal>test-jar-no-fork</goal>
							</goals>
						</execution>
					</executions>
				</plugin>

				<!-- code quality -->
				<plugin>
					<groupId>org.codehaus.mojo</groupId>
					<artifactId>sonar-maven-plugin</artifactId>
					<version>${sonar-maven-plugin.version}</version>
				</plugin>
				<plugin>
					<groupId>org.jacoco</groupId>
					<artifactId>jacoco-maven-plugin</artifactId>
					<version>0.8.8</version>
					<configuration>
						<excludes>
							<exclude>**/*Test.*</exclude>
							<exclude>**/*IT.*</exclude>
						</excludes>
						<includes>
							<include>net/nhs/**</include>
							<include>uk/nhs/**</include>
						</includes>
					</configuration>
					<executions>
						<execution>
							<id>pre-unit-test</id>
							<goals>
								<goal>prepare-agent</goal>
							</goals>
						</execution>
						<execution>
							<id>pre-integration-test</id>
							<phase>pre-integration-test</phase>
							<goals>
								<goal>prepare-agent-integration</goal>
							</goals>
						</execution>
						<execution>
							<id>report-unit-test</id>
							<phase>verify</phase>
							<goals>
								<goal>report</goal>
							</goals>
						</execution>
						<execution>
							<id>report-integration-test</id>
							<phase>verify</phase>
							<goals>
								<goal>report-integration</goal>
							</goals>
						</execution>
						<execution>
							<id>merge</id>
							<phase>verify</phase>
							<goals>
								<goal>merge</goal>
							</goals>
							<configuration>
								<destFile>${project.build.directory}/jacoco-all.exec</destFile>
								<fileSets>
									<fileSet>
										<directory>${project.build.directory}</directory>
										<includes>
											<include>**/*.exec</include>
										</includes>
									</fileSet>
								</fileSets>
							</configuration>
						</execution>
						<execution>
							<id>report-all-test</id>
							<phase>verify</phase>
							<goals>
								<goal>report</goal>
							</goals>
							<configuration>
								<dataFile>${project.build.directory}/jacoco-all.exec</dataFile>
								<outputDirectory>${project.reporting.outputDirectory}/jacoco-all</outputDirectory>
							</configuration>
						</execution>
					</executions>
				</plugin>
			</plugins>
		</pluginManagement>

		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
			</plugin>
		</plugins>
	</build>

	<profiles>

		<profile>
			<id>sonar</id>
			<properties>
				<sonar.java.codeCoveragePlugin>jacoco</sonar.java.codeCoveragePlugin>
				<sonar.surefire.reportsPath>/target/surefire-reports</sonar.surefire.reportsPath>
				<sonar.jacoco.reportPath>target/jacoco.exec</sonar.jacoco.reportPath>
				<sonar.jacoco.itReportPath>target/jacoco-it.exec</sonar.jacoco.itReportPath>
			</properties>
			<build>
				<pluginManagement>
					<plugins>
						<plugin>
							<groupId>org.codehaus.mojo</groupId>
							<artifactId>sonar-maven-plugin</artifactId>
							<version>${sonar-maven-plugin.version}</version>
						</plugin>
					</plugins>
				</pluginManagement>
				<plugins>
					<plugin>
						<groupId>org.codehaus.mojo</groupId>
						<artifactId>sonar-maven-plugin</artifactId>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
    <repositories>
		<repository>
			<id>gitlab-maven-public</id>
			<url>https://gitlab.com/api/v4/projects/42333228/packages/maven</url>
		</repository>
		<repository>
			<id>gitlab-maven-private</id>
			<url>https://gitlab.com/api/v4/projects/42333223/packages/maven</url>
		</repository>
		<repository>
			<id>gitlab-maven-application-private</id>
			<url>https://gitlab.com/api/v4/projects/46181431/packages/maven</url>
		</repository>
	</repositories>

</project>
