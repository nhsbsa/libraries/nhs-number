# NHS Number library

## Background

The NHS number was developed to support unique patient identification within the NHS. It is a unique 10-digit number. The first nine digits are the identifier and the tenth is a check digit used to confirm the number's validity. There is no significance to any of the digits. This style of numbering was introduced in 1996 replacing a variety of previous systems. The NHS number should be displayed and printed in a '3 3 4' format (e.g. 987 654 4321).

For more information about NHS number see [http://webarchive.nationalarchives.gov.uk/20160921150847/http://systems.digital.nhs.uk/nhsnumber/staff/history](http://webarchive.nationalarchives.gov.uk/20160921150847/http://systems.digital.nhs.uk/nhsnumber/staff/history)

## JSR-303 Validation

This library provides an [JSR-303](http://beanvalidation.org/1.0/spec/) annotation to help validate NHS Numbers

## Property Editor TODO

This library provides a `java.beans.PropertyEditor` to help capture and render an NHS Number on screen

## Form Fields TODO

This library provides a data object useful in binding an NHS Number to standard MVC frameworks
